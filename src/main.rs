use clap::{App, Arg};
use init_convert::{OpenrcService, RunitService, SystemdService};
use log::{debug, error};

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .about("Converts systemd service files to various inits' file structures.")
        .version(env!("CARGO_PKG_VERSION"))
        .arg(
            Arg::with_name("to")
                .about("The init that the input will be converted to.")
                .long("to-init")
                .short('t')
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("debug")
                .about("Enables debug information output.")
                .long("debug")
                .short('d'),
        )
        .arg(
            Arg::with_name("input")
                .about("Input file, contatining a systemd service.")
                .index(1)
                .required(true),
        )
        .get_matches();

    simple_logger::init_with_level(if matches.is_present("debug") {
        log::Level::Debug
    } else {
        log::Level::Warn
    })
    .unwrap();

    debug!("main to: {:?}", matches.value_of("to"));
    let init = matches.value_of("to").unwrap().to_lowercase();

    debug!("main input: {:?}", matches.value_of("input"));
    let input = matches.value_of("input").unwrap();

    let sys_serv = match SystemdService::from_file(&std::path::Path::new(&input)) {
        Ok(s) => s,
        Err(err) => abort(err),
    };
    debug!("main parsed file (systemd service): {:?}", sys_serv);

    let converted = match init.as_str() {
        "runit" => Init::Runit(RunitService::from(sys_serv)),
        "openrc" => Init::Openrc(OpenrcService::from(sys_serv)),
        _ => abort(anyhow::Error::msg("Can't convert to given init.")),
    };
    debug!("main converted file ({} service): {:?}", init, converted);

    println!("{}", converted);
}

fn abort(err: anyhow::Error) -> ! {
    error!("{}", err);
    std::process::exit(1);
}

#[derive(Debug)]
enum Init {
    Runit(RunitService),
    Openrc(OpenrcService),
}

use std::fmt::{Display, Formatter, Result};
impl Display for Init {
    fn fmt(&self, f: &mut Formatter) -> Result {
        use Init::*;
        write!(
            f,
            "{}",
            match self {
                Runit(s) => s.to_string(),
                Openrc(s) => s.to_string(),
            }
        )
    }
}
