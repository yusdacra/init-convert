use crate::SystemdService;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Default, Clone)]
pub struct RunitService {
    run: String,
    finish: String,
}

impl From<SystemdService> for RunitService {
    fn from(ss: SystemdService) -> Self {
        let run = format!(
            "#!/bin/sh\n\n\

            {}\n\
            ",
            ss.service.exec_start,
        );

        let finish = format!(
            "#!/bin/sh\n\n\

            {}\n\
            ",
            ss.service.exec_stop,
        );

        Self { run, finish }
    }
}

impl Display for RunitService {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "run:\n{}\nfinish:\n{}", self.run, self.finish)
    }
}
