use crate::SystemdService;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Default, Clone)]
pub struct OpenrcService {
    pub command: String,
    pub command_args: String,
    pub pidfile: String,
    pub name: String,
    pub desc: String,
}

impl OpenrcService {
    pub fn command(mut self, command: String) -> Self {
        self.command = command;
        self
    }

    pub fn command_args(mut self, command_args: String) -> Self {
        self.command_args = command_args;
        self
    }

    pub fn pidfile(mut self, pidfile: String) -> Self {
        self.pidfile = pidfile;
        self
    }

    pub fn name(mut self, name: String) -> Self {
        self.name = name;
        self
    }

    pub fn desc(mut self, desc: String) -> Self {
        self.desc = desc;
        self
    }
}

impl From<SystemdService> for OpenrcService {
    fn from(ss: SystemdService) -> Self {
        use log::debug;

        let mut mline = parse_systemd_exec(ss.service.exec_start);
        debug!("OpenrcService from SystemdService: mline: {:?}", mline);

        let (command, command_args) = if !mline.is_empty() {
            let mut com = crate::parse_list(&mline.pop().unwrap_or_default());
            (com.remove(0), com)
        } else {
            Default::default()
        };
        debug!(
            "OpenrcService from SystemdService: (command, command_args): ({:?}, {:?})",
            command, command_args
        );

        Self::default()
            .command(command)
            .command_args(crate::to_list(&command_args))
            .desc(ss.unit.desc)
            .pidfile(ss.service.pidfile)
    }
}

impl Display for OpenrcService {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(
            f,
            "#!/sbin/openrc-run\n\n\

            command={}\n\
            command_args={}\n\
            pidfile={}\n\n\

            name={}\n\
            description={}\n\
            ",
            self.command, self.command_args, self.pidfile, self.name, self.desc
        )
    }
}

fn parse_systemd_exec(e: String) -> Vec<String> {
    e.rsplit(|c| c == '\n' || c == ';')
        .map(|f| f.trim().to_owned())
        .collect()
}
