use super::Section;
use crate::{parse_list, to_list, Ini};

const UNIT_SECTION: &str = "Unit";
const DESC_KEY: &str = "Description";
const WANTS_KEY: &str = "Wants";
const REQUIRES_KEY: &str = "Requires";

#[derive(Debug, Default, Clone)]
pub struct Unit {
    pub desc: String,
    pub wants: Vec<String>,
    pub requires: Vec<String>,
}

impl Unit {
    pub fn desc(mut self, desc: String) -> Self {
        self.desc = desc;
        self
    }
    pub fn wants(mut self, wants: Vec<String>) -> Self {
        self.wants = wants;
        self
    }
    pub fn requires(mut self, requires: Vec<String>) -> Self {
        self.requires = requires;
        self
    }
}

impl Section for Unit {
    fn add_sec(&self, i: &mut Ini) {
        i.with_section(Some(UNIT_SECTION))
            .set(DESC_KEY, &self.desc)
            .set(WANTS_KEY, to_list(&self.wants))
            .set(REQUIRES_KEY, to_list(&self.requires));
    }

    fn get_sec(i: &Ini) -> Self {
        if let Some(sec) = i.section(Some(UNIT_SECTION)) {
            let desc = sec.get(DESC_KEY).unwrap_or_default().to_owned();
            let wants = parse_list(sec.get(WANTS_KEY).unwrap_or_default());
            let requires = parse_list(sec.get(REQUIRES_KEY).unwrap_or_default());

            Self {
                desc,
                wants,
                requires,
            }
        } else {
            Self::default()
        }
    }
}
