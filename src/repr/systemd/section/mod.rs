pub mod service;
pub mod unit;

pub use service::Service;
pub use unit::Unit;

use crate::Ini;

pub trait Section {
    fn add_sec(&self, i: &mut Ini);
    fn get_sec(i: &Ini) -> Self
    where
        Self: Sized;
}
