use super::Section;
use crate::Ini;

const SERVICE_SECTION: &str = "Service";
const TYPE_KEY: &str = "Type";
const EXEC_START_KEY: &str = "ExecStart";
const EXEC_STOP_KEY: &str = "ExecStop";
const PIDFILE_KEY: &str = "PIDFile";
const RESTART_KEY: &str = "Restart";

#[derive(Debug, Default, Clone)]
pub struct Service {
    pub r#type: String,
    pub exec_start: String,
    pub exec_stop: String,
    pub pidfile: String,
    pub restart: String,
}

impl Service {
    pub fn r#type(mut self, r#type: String) -> Self {
        self.r#type = r#type;
        self
    }

    pub fn pidfile(mut self, pidfile: String) -> Self {
        self.pidfile = pidfile;
        self
    }
    pub fn exec_start(mut self, exec_start: String) -> Self {
        self.exec_start = exec_start;
        self
    }

    pub fn exec_stop(mut self, exec_stop: String) -> Self {
        self.exec_stop = exec_stop;
        self
    }
    pub fn restart(mut self, restart: String) -> Self {
        self.restart = restart;
        self
    }
}

impl Section for Service {
    fn add_sec(&self, i: &mut Ini) {
        i.with_section(Some(SERVICE_SECTION))
            .set(TYPE_KEY, &self.r#type)
            .set(EXEC_START_KEY, &self.exec_start)
            .set(EXEC_STOP_KEY, &self.exec_stop)
            .set(PIDFILE_KEY, &self.pidfile)
            .set(RESTART_KEY, &self.restart);
    }

    fn get_sec(i: &Ini) -> Self {
        if let Some(sec) = i.section(Some(SERVICE_SECTION)) {
            let r#type = sec.get(TYPE_KEY).unwrap_or_default().to_owned();
            let exec_start = sec.get(EXEC_START_KEY).unwrap_or_default().to_owned();
            let exec_stop = sec.get(EXEC_STOP_KEY).unwrap_or_default().to_owned();
            let pidfile = sec.get(PIDFILE_KEY).unwrap_or_default().to_owned();
            let restart = sec.get(RESTART_KEY).unwrap_or_default().to_owned();

            Self {
                r#type,
                exec_start,
                exec_stop,
                pidfile,
                restart,
            }
        } else {
            Self::default()
        }
    }
}
