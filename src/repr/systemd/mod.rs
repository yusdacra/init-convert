pub mod section;

use crate::Ini;
use log::debug;
use section::{Section, Service, Unit};
use std::fmt::{Display, Formatter, Result};

#[derive(Debug, Default, Clone)]
pub struct SystemdService {
    pub name: String,
    pub unit: Unit,
    pub service: Service,
}

impl SystemdService {
    pub fn name(mut self, name: String) -> Self {
        self.name = name;
        self
    }
    pub fn unit(mut self, unit: Unit) -> Self {
        self.unit = unit;
        self
    }

    pub fn service(mut self, service: Service) -> Self {
        self.service = service;
        self
    }

    pub fn from_file(file_path: &std::path::Path) -> anyhow::Result<Self> {
        debug!(
            "SystemdService from_file: Loading file from path: {}",
            file_path.to_string_lossy()
        );
        let s = Self::from(Ini::load_from_file(file_path)?);
        Ok(s.name(
            file_path
                .file_name()
                .ok_or_else(|| anyhow::Error::msg("Could not extract file name from file path."))?
                .to_str()
                .ok_or_else(|| anyhow::Error::msg("Could not convert OsStr to str."))?
                .to_owned(),
        ))
    }
}

impl Into<Ini> for SystemdService {
    fn into(self) -> Ini {
        let mut i = Ini::new();
        self.unit.add_sec(&mut i);
        self.service.add_sec(&mut i);
        i
    }
}

impl Into<Ini> for &SystemdService {
    fn into(self) -> Ini {
        let mut i = Ini::new();
        self.unit.add_sec(&mut i);
        self.service.add_sec(&mut i);
        i
    }
}

impl From<Ini> for SystemdService {
    fn from(i: Ini) -> Self {
        SystemdService::default()
            .unit(Unit::get_sec(&i))
            .service(Service::get_sec(&i))
    }
}

impl Display for SystemdService {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let mut bufw = std::io::BufWriter::new(Vec::new());
        let ini: Ini = self.into();
        ini.write_to(&mut bufw).unwrap();
        write!(f, "{}", unsafe {
            String::from_utf8_unchecked(bufw.into_inner().unwrap())
        })
    }
}
