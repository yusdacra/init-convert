pub mod repr;

pub use ini::Ini;
pub use repr::*;

pub fn ini_to_string(ini: &Ini) -> String {
    let mut bufw = std::io::BufWriter::new(Vec::new());
    ini.write_to(&mut bufw).unwrap();
    unsafe { String::from_utf8_unchecked(bufw.into_inner().unwrap()) }
}

pub(crate) fn parse_list(s: &str) -> Vec<String> {
    s.split_whitespace().map(|f| f.to_string()).collect()
}

pub(crate) fn to_list(s: &[String]) -> String {
    let mut res = String::new();
    s.iter().for_each(|f| {
        res.push_str(f);
        res.push(' ');
    });
    res.trim().to_owned()
}
